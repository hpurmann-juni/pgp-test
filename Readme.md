# PGP Test

This repository reproduces encrypting a test file with a given PGP key. Steps to reproduce:


## On your host
1. Clone repository
1. Store public key file used for encryption in repository. Rename it to `key.asc`
1. Build docker image and run a container:
    ```bash
    docker build -t mygnupg .
    docker run -it --name mygnupgcontainer mygnupg bash
    ```

## In the container
1. In the container, import the key
    ```bash
    gpg --import key.asc
    ```
1. List keys and get key id, e.g. starting with `E369AD74...`
    ```bash
    gpg --list-keys
    ```
1. Use key id to encrypt file
    ```bash
    gpg -e -r <KEYID> test.txt
    ```
1. You will get a warning that the key is not trusted. Accept this with `y`


## Back on your host

1. Copy the encrypted file out of the container
    ```bash
    docker cp mygnupgcontainer:/test.txt.gpg .
    ```

A new encrypted file will appear on your host.
